package com.patterns.examples.state;
//Nastya Gavrilova
public class StatePatternExample {
    public static void main(String[] args) {
        Door myDoor = new Door(new Open());

        for (int i = 0; i < 6; i++) {
            myDoor.move();
            myDoor.changeState();
        }
    }

}